/*
 * Copyright (c) 2015, salesforce.com, inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 * following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * Neither the name of salesforce.com, inc. nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import React from 'react';
import {
    ScrollView,
    StyleSheet,
    View,
    TouchableHighlight,
    Text,
    Dimensions,
    Image,
    FlatList,
    Navigator,
    Alert,
    Button,
    TextInput
} from 'react-native';



import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';

import RetroMapStyles from './MapStyles/RetroMapStyles.json';
import DarkMapStyles from './MapStyles/DarkMapStyles.json';

import {oauth, net} from 'react-native-force';

import Field from './Field';
import storeMgr from './StoreMgr';
import NavigationExperimental from 'react-native-deprecated-custom-components';


import flagPinkImg from './assets/flag-pink.png';

//let navigator;

let { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

var createReactClass = require('create-react-class');

const HelloScreen = createReactClass({
    onRegionChange(region) {
      this.setState({ region });
    },
    getInitialState() {


       // navigator = NavigationExperimental.navigator;

        //navigator =this.props.navigator;
        //console.log('getInitialState!!!!! latitude ' + this.props.lat);
        //console.log('getInitialState!!!!! longitude  ' + this.props.lon);

        return {

            hello: this.props.hello,
            region: {
                  latitude: 37.78825,
                  longitude: -122.4324,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                },
            data: [],
            markers: [
                {
                  coordinate: {latitude: 40.4664, longitude: -3.7020},
                  title:'This is a native view',
                  description:'Lorem ipsum dolor sit amet',
                  key: 0
                },
                {
                  coordinate: {latitude: 40.4760,longitude: -3.8020},
                  title:'This is a native view',
                  description:'Lorem ipsum dolor sit amet',
                  key:1
                }
              ],
              isShowingText: true,
              toggle: false,
              myAccount:{id:'',Name:''}

        };
    },
    componentDidMount() {

        /*setInterval(() => {
              this.setState(previousState => {
                return { isShowingText: !previousState.isShowingText };
              });
            }, 10000);*/


            navigator.geolocation.getCurrentPosition(
              (position) => {
                this.setState(
                  {
                    region:{
                      latitude: position.coords.latitude,
                      longitude: position.coords.longitude,
                      latitudeDelta: 0.0922,
                      longitudeDelta: 0.0421
                    }
                });
              },
              (error) => this.setState({ error: error.message }),
              { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
            );

        console.log('componentDidMount!!!!!');
        var that = this;
        oauth.getAuthCredentials(
            () => that.fetchData(), // already logged in
            () => {
                oauth.authenticate(
                    () => that.fetchData(),
                    (error) => console.log('Failed to authenticate:' + error)
                );
            });
    },
    fetchData() {
        console.log('fetchData!!!!!');
        var that = this;
        net.query('SELECT Id, BillingAddress, BillingCity, BillingCountry, BillingPostalCode, BillingState, BillingStreet, Name, Geolocation__Latitude__s, Geolocation__Longitude__s  FROM Account where isPersonAccount = false LIMIT 10',
                  (response) => that.setState({data: response.records}),
                  (error) => console.log( error)
                 );
    },

    toggleTrue(selecctedIndex) {
      console.log('toggle '+ selecctedIndex);

      this.setState(
                  {
                    myAccount:{
                      Id: this.state.data[selecctedIndex].Id,
                      Name: this.state.data[selecctedIndex].Name,
                      BillingCity: this.state.data[selecctedIndex].BillingCity,
                      BillingCountry: this.state.data[selecctedIndex].BillingCountry,
                      BillingPostalCode: this.state.data[selecctedIndex].BillingPostalCode,
                      BillingState: this.state.data[selecctedIndex].BillingState,
                      BillingStreet: this.state.data[selecctedIndex].BillingStreet,
                    }
                });


      return this.setState({toggle: true});
    },

    PressButton(accountId) {
        //Alert.alert('You tapped the button!')

      this.props.nav.push({
            name: 'Account',
            account:accountId

        });



      },

    render() {

      var markers = new Array();

      if (this.state.data[0] != null)
      {
          for (var i = 0; i < this.state.data.length; i++)
          {
              var marker = {};
              marker.title = this.state.data[i].Name;
              marker.coordinate= {};
              marker.coordinate.latitude = this.state.data[i].Geolocation__Latitude__s;
              marker.coordinate.longitude = this.state.data[i].Geolocation__Longitude__s;
              marker.key = i;

              markers.push(marker);

          }


      }

      let pic = {
        uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
      };

      /*console.log('*********** ' + this.state.markers[0].coordinate.latitude );
      console.log('*********** ' + this.state.markers[0].coordinate.longitude );
      console.log('*********** ' + this.state.region.latitude );
      console.log('*********** ' + this.state.region.longitude );*/
/*
      <Text>Hello world! Jose  </Text>
              <Text>Account List</Text>
         <FlatList
                data={this.state.data}
                renderItem={({item}) => <Text  style={stylesTab.item}>{item.Name} </Text>}
                keyExtractor={(item, index) => index}/>*/
    
    return (

      <View style={{paddingTop: 56, flex: 1}}>
      
        <MapView
          provider={ PROVIDER_GOOGLE }
          style={[stylesMap.container, this.state.toggle && stylesMap.containerAlt]}  
          showsUserLocation={ true }
          customMapStyle={ RetroMapStyles }
          region={this.state.region}
          >

        {markers.map(marker => (
            <Marker
              title={marker.title}
              coordinate={marker.coordinate}
              image={flagPinkImg}
              key={marker.key}
              onPress={(e) => this.toggleTrue(marker.key)}
              
            />
          ))}

  

          </MapView>
          <View style={{paddingTop: 10, paddingLeft: 10}}>
            <Text style={stylesText.titleText}>{this.state.myAccount.Name}</Text>
            <Text style={stylesText.baseText}>{this.state.myAccount.BillingStreet} {this.state.myAccount.BillingPostalCode}</Text>
            <Text style={stylesText.baseText}>{this.state.myAccount.BillingCity} {this.state.myAccount.BillingCity}</Text>
            <Text> </Text>
          </View>

          <Button
            onPress={(e) => this.PressButton(this.state.myAccount.Id)}
            title="Ver Comentarios !!!!!!"
            color="#ff8200"
          />

      </View>
    );
  }
});

const stylesMap = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%'
  },
  containerAlt: {
    height: '75%',
    width: '100%'
  },
});

const stylesTab = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
        backgroundColor: 'white',
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    }
});

const stylesText = StyleSheet.create({
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default HelloScreen;
