/*
 * Copyright (c) 2015, salesforce.com, inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 * following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * Neither the name of salesforce.com, inc. nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import React from 'react';
import {
    ScrollView,
    StyleSheet,
    View,
    TouchableHighlight,
    Text,
    TextInput,
    Button, Image,

} from 'react-native';


import NavigationExperimental from 'react-native-deprecated-custom-components';
import {oauth, net, util} from 'react-native-force';

import styles from './styles';
import Comments from 'react-native-comments';
import * as commentActions from './ExampleActions';
import moment from 'moment';

import taller from './assets/ASB_2466.jpg';

let navigator;


var createReactClass = require('create-react-class');
var scrollIndex = 0;
var navigatorStyle = {};

var mioauth;

var actions = commentActions;

var miUsuario;

const AccountScreen = createReactClass({





    getInitialState() {
        console.log('Account Screen');
        console.log(this.props.account);
        //navigator = NavigationExperimental.navigator;
        return {
            account: this.props.account,
            data: [],
            comments: [],
                  loadingComments: true,
                  lastCommentUpdate: null,
                  review: this.props.review ? this.props.review : null,
                  login: null,
                  id: this.props.id



            
        };
    },


    componentDidMount() {

        console.log('componentDidMount!!!!!');
        var that = this;
        mioauth = oauth.getAuthCredentials(
            (response) => that.fetchData(response), // already logged in
            () => {
                oauth.authenticate(
                    () => that.fetchData(),
                    (error) => console.log('Failed to authenticate:' + error)
                );
            });
    },

    fetchData(response) {
        
        console.log('fetchData!!!!!');
        console.log(response);
        var that = this;
        var query = 'SELECT Comment__c,CreatedDate,UserName__c,Experience__c,Garage__c,Id,Name,Score__c FROM Evaluation__c where Garage__c = \'' + this.state.account+'\'';
        
        

        var queryUser = 'SELECT id, Name  FROM User where id= \'' + response.userId+'\'';
        console.log(queryUser);

        net.query(query,
                  (response) => that.processData(response.records),
                  (error) => console.log( error)
                 );
        net.query(queryUser,
                  (response) => that.processDataUser(response.records),
                  (error) => console.log( error)
                 );
    },

    processDataUser(records){
      console.log('processDataUser');
      console.log(records);
      console.log(records[0].Name);
      miUsuario = records[0].Name;

    },

    processData(records){
        this.setState({data: records});
        console.log(records);
        console.log(records.length);


      if (records != null)
      {
          var coms = new Array();

          for (var i = 0; i < records.length; i++)
          {
              var com = {};
              com.parentId = '';
              com.commentId= records[i].Id;
              com.name= records[i].Name;
              com.created_at= records[i].CreatedDate;
              com.email= records[i].UserName__c;
              com.body= records[i].Comment__c;
              com.likes= [
                  ];
              coms.push(com);
          }
          console.log(coms);
          this.setState({comments: coms});


      }






    },


    PressButton(accountId) {
        //Alert.alert('You tapped the button!')

        console.log('press ' + accountId);
        const uniq = Math.floor(Math.random() * 1000000);
        const firstName = 'First_' + uniq;
        const lastName = 'Last_' + uniq;
        var contactId;


        
        net.create('contact', {FirstName: firstName, LastName: lastName}, 
                  (response) => console.log( 'OK'),
                  (error) => console.log(error));
            


      },

    componentWillMount(){
        const c = this.state.comments;
        this.setState({
          comments : c,
          loadingComments: false,
          lastCommentUpdate: new Date().getTime()
    })
  },

  extractUsername (c) {
    try {
      return c.email  !== '' ? c.email : null
    } catch (e) {
      console.log(e)
    }
  },
  extractBody (c) {
    try {
      return c.body && c.body !== '' ? c.body : null
    } catch (e) {
      console.log(e)
    }
  },
  extractImage (c) {

    try {
      return c.image_id && c.user.image_id !== '' ? c.user.image_id
        : 'https://ireview.live/img/no-user.png'

    } catch (e) {
      console.log(e)
    }
  },
  extractChildrenCount (c) {
    try {
      return c.childrenCount || 0
    } catch (e) {
      console.log(e)
    }
  },
  extractEditTime (item) {
    try {
      return item.updated_at
    } catch (e) {
      console.log(e)
    }
  },
  extractCreatedTime (item) {
    try {
      return item.created_at
    } catch (e) {
      console.log(e)
    }
  },
  likeExtractor (item) {
    return item.liked
  },
  reportedExtractor (item) {
    return item.reported
  },
  likesExtractor (item) {

    return item.likes.map((like) => {
      return {
        image: like.image,
        name: like.username,
        user_id: like.user_id,
        tap: (username) => {
          console.log('Taped: '+username)
        }
      }
    })

  },
  isCommentChild(item){
    return  item.parentId !== null
  },



    

    render() {
        const review = this.state.review
    const data = this.state.comments

    return (
      /*
      * They should add scroll to end on save action
      *They should not update comments if there are modals opened
      *
      * */
      <ScrollView style={styles.container}

                  onScroll={(event) => {
                    this.scrollIndex = event.nativeEvent.contentOffset.y
                  }}
                  ref={'scrollView'}>
                  <View style={{paddingTop: 56, flex: 1}}></View>
        <Image style={{height: 200}}
               source={require('./assets/ASB_2466.jpg')}/>

        {this.state.comments.length ? <Comments
          data={data}
          //To compare is user the owner
          viewingUserName={'testUser'}
          //how many comments to display on init
          initialDisplayCount={5}
          //How many minutes to pass before locking for editing
          editMinuteLimit={0}

          //What happens when user taps on username or photo
          usernameTapAction={(username) => {
            console.log('Taped user: '+username)
          }}
          //Where can we find the children within item.
          //Children must be prepared before for pagination sake
          childPropName={'children'}
          isChild={(item) =>this.isCommentChild(item)}
          //We use this for key prop on flat list (i.e. its comment_id)
          keyExtractor={item => item.commentId}
          //Extract the key indicating comments parent
          parentIdExtractor={item=>item.parentId}
          //what prop holds the comment owners username
          usernameExtractor={item => this.extractUsername(item)}
          //when was the comment last time edited
          editTimeExtractor={item => this.extractEditTime(item)}
          //When was the comment created
          createdTimeExtractor={item => this.extractCreatedTime(item)}
          //where is the body
          bodyExtractor={item => this.extractBody(item)}
          //where is the user image
          imageExtractor={item => this.extractImage(item)}
          //Where to look to see if user liked comment
          likeExtractor={item => this.likeExtractor(item)}
          //Where to look to see if user reported comment
          reportedExtractor={item => this.reportedExtractor(item)}
          //Where to find array with user likes
          likesExtractor={item => this.likesExtractor(item)}
          //Where to get nr of replies
          childrenCountExtractor={item => this.extractChildrenCount(item)}

          //what to do when user clicks reply. Usually its header height + position (b/c scroll list heights are relative)
          replyAction={offset => {
            this.refs.scrollView.scrollTo({x: null, y: this.scrollIndex + offset - 300, animated: true})

          }}
          //what to do when user clicks submits edited comment
          saveAction={(text, parentCommentId) => {
            let date = moment().format('YYYY-MM-DD HH:mm:ss');
            let comments = actions.save(this.state.comments, text, parentCommentId, date, miUsuario)
            this.setState({
              comments: comments})

            if(!parentCommentId){
              this.refs.scrollView.scrollToEnd()
            }
            console.log('SAVE ACTION !!!');

            net.create('Evaluation__c', {Comment__c: text, Garage__c: this.state.account, UserName__c: miUsuario, Name:text}, 
                  (response) => console.log( 'OK'),
                  (error) => console.log(error));

          }}

          //what to do when user clicks submits edited comment
          editAction={(text, comment) => {
            let comments = actions.edit(this.state.comments, comment, text)
            this.setState({
              comments: comments})
          }}

          //what to do when user clicks report submit
          reportAction={(comment) => {
            let comments = actions.report(this.state.comments, comment)
            this.setState({
              comments: comments})
          }

          }
          //what to do when user clicks like
          likeAction={(comment) => {
            let comments = actions.like(this.state.comments, comment)
            this.setState({
              comments: comments})
          }
          }
          //Must return promise
          paginateAction={(from_comment_id, direction, parent_comment_id) => {
            //Must return array of new comments after pagination

            let newComments = actions.paginateComments(
              this.state.comments,
              from_comment_id,
              direction,
              parent_comment_id)

            this.setState({
              comments: newComments})
            let self = this
            setTimeout(function () {
              if(direction == 'up') {
                self.refs.scrollView.scrollTo({x: 0, y: 500, animated: true})
              }else{
                self.refs.scrollView.scrollTo({x: 0, y: 0, animated: true})
              }
            }, 3000)

          }
          }
        /> : null}

      </ScrollView>

    )
    


    }
});

const stylesBut = StyleSheet.create({
    button: {
        fontSize: 16,
        color: 'red',
        padding: 5
    }
});

export default AccountScreen;
